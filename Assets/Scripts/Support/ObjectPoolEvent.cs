﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolEvent : MonoBehaviour {

    public bool IsDestroyed = false;
    private ObjectPool _pool;

    public void SetHandler(ObjectPool pool) {
        _pool = pool;
    }

    private void OnDisable() {
        if(!IsDestroyed)
            _pool.ObjectDeactived(gameObject);
    }
}
