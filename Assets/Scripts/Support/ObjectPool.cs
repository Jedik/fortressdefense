﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool {

    private List<GameObject> _activeObjects = new List<GameObject>();
    private List<GameObject> _hiddenObjects = new List<GameObject>();

    private string _path;
    private GameObject prefab;

    public ObjectPool(string path, bool prepare = false) {
        prefab = Resources.Load<GameObject>(path);
        if (prepare) {
            GameObject tmp = MonoBehaviour.Instantiate(prefab, Vector3.zero, Quaternion.identity);
            _hiddenObjects.Add(tmp);
            tmp.AddComponent<ObjectPoolEvent>().SetHandler(this);
        }
    }


    public GameObject GetObject(Vector3 position) {
        if (_hiddenObjects.Count == 0) {
            GameObject tmp = MonoBehaviour.Instantiate(prefab, position, Quaternion.identity);
            _activeObjects.Add(tmp);
            tmp.AddComponent<ObjectPoolEvent>().SetHandler(this);
            return tmp;
        } else {
            GameObject tmp = _hiddenObjects[0];
            _hiddenObjects.RemoveAt(0);
            _activeObjects.Add(tmp);
            tmp.transform.position = position;
            return tmp;
        }
    }

    public void ObjectDeactived(GameObject gameObject) {
        _activeObjects.Remove(gameObject);
        _hiddenObjects.Add(gameObject);
    }

    public void HideAll() {
        for (int counter = _activeObjects.Count - 1; counter >= 0; counter--) {
            _activeObjects[counter].GetComponent<EnemyController>().Restart();
        }
    }
}
