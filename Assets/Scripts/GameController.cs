﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public GameUIController UI;
    public FortController Fort;


    private EnemySpawner _spawner;

    private int _killsCounter = 0;
    private int enemies = 20;
    private bool _isPaused;

	// Use this for initialization
	void Start () {

        if (_spawner == null)
            _spawner = gameObject.AddComponent<EnemySpawner>();
        _spawner.Init(UI.GetSpawnPosition(), this);

        StartGame();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0) && !_isPaused) {
            Vector2 rayPos = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
            RaycastHit2D hit = Physics2D.Raycast(rayPos, Vector2.zero, 0f);

            if (hit.collider != null) {
                Fort.Attack(hit.point);
            }

        }
    }

    public void StartGame() {
        Fort.StartGame(UI.GetFortPosition());
        UI.DrawHPBar(1);
        _spawner.StartSpawn(enemies);
        Time.timeScale = 1;
        _isPaused = false;
    }

    public void FortDamage(float damage) {
        float tmp = Fort.GotDamage(damage);
        UI.DrawHPBar(tmp);
        if (tmp <= 0) {
            Lose();
        }
    }


    private void Lose() {
        Time.timeScale = 0;
        _isPaused = true;
        UI.ShowWinLoseScreen(0);
    }

    private void Win() {
        Time.timeScale = 0;
        _isPaused = true;
        float tmp = Fort.GotDamage(0);
        if (tmp == 1) {
            UI.ShowWinLoseScreen(3);
        } else if (tmp > 0.5f) {
            UI.ShowWinLoseScreen(2);
        } else {
            UI.ShowWinLoseScreen(1);
        }
    }

    public void EnemyDied() {
        _killsCounter++;
        UI.UpdateKillCounter(_killsCounter);
        if (_killsCounter == enemies && !_isPaused) {
            Win();
        }
    }

    public void Restart() {
        _killsCounter = 0;
        UI.UpdateKillCounter(_killsCounter);
        _spawner.Restart();
        StartGame();
    }

    public void Quit() {
        Application.Quit();
    }
}
