﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class GameUIController : MonoBehaviour {

    [SerializeField]
    private GameController _gameController;
    [SerializeField]
    private Transform _fortPosition;
    [SerializeField]
    private Transform _spawnPosition;
    [SerializeField]
    private Image _hpBar;
    [SerializeField]
    private Text _killCounter;
    [SerializeField]
    private SpriteAtlas _atlas;
    [SerializeField]
    private GameObject _winLoseScreen;
    [SerializeField]
    private Image _stars;
    [SerializeField]
    private Text _winLoseText;


    public Vector3 GetFortPosition() {
        return Camera.main.ScreenToWorldPoint(new Vector3(_fortPosition.position.x, _fortPosition.position.y, 20));
    }

    public Vector3 GetSpawnPosition() {
        return Camera.main.ScreenToWorldPoint(new Vector3(_spawnPosition.position.x, _spawnPosition.position.y, 19));
    }

    public void ShowWinLoseScreen(int value) {
        _winLoseScreen.SetActive(true);
        if (value == 0) {
            _winLoseText.text = "YOU LOSE!";
            _stars.sprite = _atlas.GetSprite(value + "Star");
        } else {
            _winLoseText.text = "YOU WIN!";
            _stars.sprite = _atlas.GetSprite(value + "Star");
        }
    }

    public void DrawHPBar(float value) {
        _hpBar.fillAmount = value;
    }

    public void UpdateKillCounter(int value) {
        _killCounter.text = value.ToString();
    }

    public void RestartButton() {
        _winLoseScreen.SetActive(false);
        _gameController.Restart();
    }

    public void QuitButton() {
        _winLoseScreen.SetActive(false);
        _gameController.Quit();
    }


}