﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class FortView : MonoBehaviour {

    public SpriteRenderer View;

    public SpriteAtlas Atlas;

	// Use this for initialization
	void Start () {
		
	}

    public void StartGame(Vector3 position) {
        View.sprite = Atlas.GetSprite("1");
        SetPosition(position);
    }

    public void SetPosition(Vector3 position) {
        View.gameObject.transform.position = position; 
    }

    public void ChangeSprite(string spriteName) {
        View.sprite = Atlas.GetSprite(spriteName);
    }
} 
