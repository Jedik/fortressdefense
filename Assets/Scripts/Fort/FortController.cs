﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FortController : MonoBehaviour {

    public FortConfig Config;
    public FortView View;
    public ArcherController[] archers = new ArcherController[3];

    private float currentHealth;

    public void StartGame(Vector3 position) {
        View.StartGame(position);
        currentHealth = Config.MaxHealth;

        foreach (var archer in archers) {
            archer.Init();
        }
    }

    public float GotDamage(float damage) {
        currentHealth -= damage;
        FortSprite(currentHealth / Config.MaxHealth);
        return currentHealth / Config.MaxHealth;
    }

    private void FortSprite(float coef) {
        if (coef >= 0.67) {
            View.ChangeSprite("1");
        } else if (coef >= 0.34) {
            View.ChangeSprite("2");
        } else if (coef > 0) {
            View.ChangeSprite("3");
        } else {
            View.ChangeSprite("4");
        }

    }

    public void Attack(Vector2 position) {
        foreach (var archer in archers) {
            archer.Attack(position);
        }
    }






}
