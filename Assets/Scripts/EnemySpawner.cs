﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    private Vector3 _spawnPoint;
    private GameController _gameController;

    private ObjectPool _batSword;
    private ObjectPool _bomber;
    private ObjectPool _onager;

    private int _totalEnemies;

    public void Init(Vector3 spawnPoint, GameController gameController) {
        _spawnPoint = spawnPoint;
        _gameController = gameController;
        
        _batSword = new ObjectPool("Prefabs/BatSword");

        _onager = new ObjectPool("Prefabs/Onager");
    }


    public void StartSpawn(int count) {
        _totalEnemies = count;
        StartCoroutine("SpawnUnits");
    }

    public void Restart() {
        StopCoroutine("SpawnUnits");
        _batSword.HideAll();
        _onager.HideAll();
    }

    private void SpawnBatSword() {
        float _scatter = Random.Range(0, -1.4f);

        _batSword.GetObject(new Vector3(_spawnPoint.x, _spawnPoint.y + _scatter, _spawnPoint.z + _scatter)).GetComponent<EnemyController>().Init(_gameController);
    }

    private void SpawnOnager() {
        float _scatter = Random.Range(0, -1.4f);

        _onager.GetObject(new Vector3(_spawnPoint.x, _spawnPoint.y + _scatter, _spawnPoint.z + _scatter)).GetComponent<EnemyController>().Init(_gameController);
    }

    private IEnumerator SpawnUnits() {
        while (_totalEnemies > 0) {
            int rnd = Random.Range(0, 2);
            _totalEnemies--;
            switch ((EnemyTypes)rnd) {
                case EnemyTypes.BatSword:
                    SpawnBatSword();
                    break;
                case EnemyTypes.Bomber:
                    break;
                case EnemyTypes.Onager:
                    SpawnOnager();
                    break;
            }
            
            
            yield return new WaitForSeconds(2);
        }
    }
}
