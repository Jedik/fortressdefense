﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherView : MonoBehaviour {

    private Animator _animator;

    public void Init() {
        _animator = GetComponent<Animator>();

    }

    public void PlayAttackAnim() {
        _animator.SetTrigger("Attack");
    }
}
