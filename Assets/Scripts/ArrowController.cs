﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour {

    private Vector3 _target;
    private bool _move = false;
    private int _damage = 0;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Enemies") {
            _move = false;
            gameObject.SetActive(false);
            collision.gameObject.GetComponent<EnemyController>().GotDamage(_damage);
        }
    }

    public void Launch(Vector3 position, int damage) {
        _target = position;
        _move = true;
        _damage = damage;
    }

    public void Update() {
        if (_move) {
            float distance = Vector3.Distance(_target, transform.position);
            if (distance > 1.1) {
                Vector3 dir = _target - transform.position;

                transform.right = Vector2.Lerp(transform.right, dir, 9 / distance * Time.deltaTime);
                transform.Translate(Vector3.right * 10 * Time.deltaTime);
            } else {
                _move = false;
                Miss();
            }


        }
    }

    private void Miss() {
        GetComponent<BoxCollider2D>().enabled = false;
        StartCoroutine("Hide");
    }

    IEnumerator Hide() {
        yield return new WaitForSeconds(1);
        gameObject.SetActive(false);
    }

}
