﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyView : MonoBehaviour {

    protected Animator _animator;

    public abstract void Init();

    public abstract void PlayAttackAnim();

    public abstract void PlayDieAnim();

    public abstract void PlayMoveAnim();
}
