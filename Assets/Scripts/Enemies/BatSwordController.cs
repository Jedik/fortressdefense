﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatSwordController : EnemyController {

    private EnemyStates _state = EnemyStates.Hidden;
    private int health;


    public override void AttackEvent(int value) {
        _gameController.FortDamage(_config.AttackPower);
    }

    public override void DiedEvent(int value) {
        StateHidden();
    }


    public override void Init(GameController gameController) {
        if (!_isInited) {
            _gameController = gameController;
            _view = GetComponent<BatSwordView>();
            _view.Init();
            _config = GetComponent<BatSwordConfig>();
        }
        _isInited = true;
        Launch();
    }

    protected override void Launch() {
        health = _config.Health;
        StateMove();
    }


    protected override void SetState(EnemyStates state) {
        switch (_state) {
            case EnemyStates.Move:
                StopCoroutine("Moving");
                break;
            case EnemyStates.Attack:
                break;
            case EnemyStates.Hidden:
                gameObject.SetActive(true);
                break;
            case EnemyStates.Die:
                GetComponent<BoxCollider2D>().enabled = true;
                break;
            default:
                break;
        }

        _state = state;
    }

    protected override void StateAttack() {
        switch (_state) {
            case EnemyStates.Move:
                StopCoroutine("Moving");
                break;
            case EnemyStates.Hidden:
                return;
            case EnemyStates.Die:
                return;
            case EnemyStates.Attack:
                return;
        }

        SetState(EnemyStates.Attack);
        Attack();
    }

    protected override void StateHidden() {
        switch (_state) {
            case EnemyStates.Hidden:
                return;
            default:
                break;
        }

        SetState(EnemyStates.Hidden);
        gameObject.SetActive(false);
    }

    protected override void StateDie() {
        switch (_state) {
            case EnemyStates.Move:
                StopCoroutine("Moving");
                break;
            case EnemyStates.Attack:
                break;
            default:
                return;
        }

        SetState(EnemyStates.Die);
        GetComponent<BoxCollider2D>().enabled = false;
        Die();
        _gameController.EnemyDied();
    }

    protected override void StateMove() {
        switch (_state) {
            case EnemyStates.Hidden:
                break;
            default:
                return;
        }

        SetState(EnemyStates.Move);
        Move();
        StartCoroutine("Moving");
    }



    IEnumerator Moving() {
        while(true) {
            transform.Translate(Vector3.right * _config.Speed * Time.deltaTime);
            yield return null;
        }
    }

    private void Attack() {
        _view.PlayAttackAnim();
    }

    private void Move() {
        _view.PlayMoveAnim();
    }

    private void Die() {
        _view.PlayDieAnim();
    }

    protected override void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == "Fort") {
            StateAttack();
        }
    }

    public override void GotDamage(int damage) {
        health -= damage;
        if (health <= 0) {
            StateDie();
        }
    }

    public override void Restart() {
        StateHidden();
    }
}
