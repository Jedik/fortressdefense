﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatSwordView : EnemyView {

    public override void Init() {
        _animator = GetComponent<Animator>();
    }

    public override void PlayAttackAnim() {
        _animator.SetTrigger("Attack");
    }

    public override void PlayDieAnim() {
        _animator.SetTrigger("Died");
    }

    public override void PlayMoveAnim() {
        _animator.SetTrigger("Resurrect");
    }
}
