﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyConfig : MonoBehaviour {

    public int Speed;
    public int AttackPower;
    public int Health;
}
