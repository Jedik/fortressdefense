﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class  EnemyController : MonoBehaviour {

    protected EnemyConfig _config;

    protected EnemyView _view;

    protected bool _isInited = false;

    protected GameController _gameController;


    public abstract void Init(GameController gameController);

    public abstract void AttackEvent(int value);

    public abstract void DiedEvent(int value);

    public abstract void GotDamage(int damage);

    public abstract void Restart();

    protected abstract void Launch();

    protected abstract void StateHidden();

    protected abstract void StateMove();

    protected abstract void StateAttack();

    protected abstract void StateDie();

    protected abstract void SetState(EnemyStates state);

    protected abstract void OnTriggerEnter2D(Collider2D collider);

}
