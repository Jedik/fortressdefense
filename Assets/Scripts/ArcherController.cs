﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherController : MonoBehaviour {

    public GameObject ArrowPoint;

    private ArcherStates _state = ArcherStates.Hidden;
    private ArcherView _view;
    private ArcherConfig _config;
    private ObjectPool _arrows;
    private Vector3 _target;

    public void Init() {
        _view = GetComponent<ArcherView>();
        _view.Init();
        _config = GetComponent<ArcherConfig>();
        _arrows = new ObjectPool("Prefabs/Arrow", true);
        StateReady();
    }


    private void SetState(ArcherStates state) {
        switch (_state) {
            case ArcherStates.Hidden:
                gameObject.SetActive(true);
                break;
        }

        _state = state;
    }


    private void StateHidden() {
        SetState(ArcherStates.Hidden);

        gameObject.SetActive(false);
    }

    private void StateReady() {
        switch (_state) {
            case ArcherStates.Ready:
                return;
        }

        SetState(ArcherStates.Ready);
    }

    private void StateAttack(Vector2 target) {
        switch (_state) {
            case ArcherStates.Attack:
                return;          
            case ArcherStates.Hidden:
                return;
        }

        SetState(ArcherStates.Attack);
        _target = target;
        _view.PlayAttackAnim();
    }

    private void AnimAttackEvent() {
        GameObject arrow = _arrows.GetObject(ArrowPoint.transform.position);
        arrow.transform.rotation = Quaternion.Euler(0, 180, 0);
        arrow.GetComponent<BoxCollider2D>().enabled = true;
        arrow.GetComponent<ArrowController>().Launch(_target, _config.AttackPower);
        arrow.SetActive(true);
        
    }

    private void AnimEndAttack() {
        StateReady();
    }

    public void Attack(Vector2 target) {
        StateAttack(target);
    }

}
