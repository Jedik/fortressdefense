﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyStates { Move, Attack, Die, Hidden }
